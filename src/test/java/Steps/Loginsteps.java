/*package Steps;

import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;

import cucumber.api.java.en.But;
import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;

public class Loginsteps {
	ChromeDriver driver;
@Given("open the browser")
public void openTheBrowser() {
	System.setProperty("webdriver.chrome.driver",
			"./drivers/chromedriver.exe");
	driver = new ChromeDriver();
}

@Given("maximize the browser")
public void maximizeTheBrowser() {
	driver.manage().window().maximize();
}

@Given("load the url")
public void loadTheUrl() {
	driver.navigate().to("http://leaftaps.com/opentaps");
}

@Given("enter the username as (.*)")
public void enterTheUsername(String Uname) {
	WebElement userName = driver.findElementById("username");
	userName.sendKeys(Uname);
}

@Given("enter the password as (.*)")
public void enterThePassword(String Password) {
	driver.findElementById("password").sendKeys(Password);
}

@Given("click on the login btn")
public void clickOnTheLoginBtn() {
	driver.findElementByClassName("decorativeSubmit").click();
}

@Given("click on crmsfa link")
public void clickOnCrmSfaLink() {
	driver.findElementByLinkText("CRM/SFA").click();
}

@Given("click on leads")
public void clickOnLeads() {
	driver.findElementByLinkText("Leads").click();
}

@Given("click on create leads")
public void clickOnCreateLeads() {
	driver.findElementByLinkText("Create Lead").click();
}

@Given("enter the first name as (.*)")
public void enterTheFirstName(String Fname) {
	driver.findElementById("createLeadForm_firstName").sendKeys(Fname);
}

@Given("enter the last name as (.*)")
public void enterTheLastName(String Lname) {
	driver.findElementById("createLeadForm_lastName").sendKeys(Lname);
}

@Given("enter the company name as (.*)")
public void enterTheCompanyName(String Cname) {
	driver.findElementById("createLeadForm_companyName").sendKeys(Cname);
}

@When("click on submit")
public void clickOnSubmit() {
	driver.findElementByClassName("smallSubmit").click();
}

@Then("verify the firstname")
public void verifyTheFirstname() {
	String Fname = driver.findElement(By.className("tabletext")).getText();
	System.out.println(Fname.contentEquals("Sang"));
	
}
@But("verify firstname")
public void firstname() {
System.out.println("failed");
}
}
*/