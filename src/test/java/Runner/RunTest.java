package Runner;

import org.junit.runner.RunWith;

import cucumber.api.CucumberOptions;
import cucumber.api.junit.Cucumber;

@RunWith(Cucumber.class)
@CucumberOptions(features="src\\test\\java\\features\\CreateLead.feature",
glue= {"com.yalla.pages","Steps"}, 
monochrome = true,
dryRun=false, tags="@smoke")
public class RunTest {
	

}
