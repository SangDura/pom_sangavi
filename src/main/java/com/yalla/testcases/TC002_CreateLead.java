package com.yalla.testcases;

import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;

import com.yalla.pages.CreateLead;
import com.yalla.pages.HomePage;
import com.yalla.pages.LeadPage;
import com.yalla.pages.LoginPage;
import com.yalla.pages.MyHomePage;
import com.yalla.pages.ViewLead;
import com.yalla.testng.api.base.Annotations;

public class TC002_CreateLead extends Annotations{
	
	@BeforeTest
	public void setData() {
		testcaseName = "TC002_CreateLead";
		testcaseDec = "Create Lead";
		author = "Gayatri";
		category = "smoke";
		excelFileName = "TC001";
	} 

	@Test(dataProvider="fetchData") 
	public void loginAndLogout(String uName, String pwd, String Fname,String Lname,String Cname) {
		new LoginPage()
		.enterUserName(uName)
		.enterPassWord(pwd) 
		.clickLogin();
		//.clickLogout();
		new HomePage()
		.clickCrmSfa();
		new MyHomePage()
		.clickLead();
		new LeadPage()
		.clickCreateLead();
		new CreateLead()
		.enterFName(Fname)
		.enterLName(Lname)
		.enterCompany(Cname)
		.clickCreateLead();
		new ViewLead()
		.VerifyFname(Fname);
		
		
		
		
		/*LoginPage lp = new  LoginPage();
		lp.enterUserName();
		lp.enterPassWord();*/
		
	}
	
}






