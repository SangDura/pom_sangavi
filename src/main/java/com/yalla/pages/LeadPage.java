package com.yalla.pages;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;
import org.openqa.selenium.support.PageFactory;

import com.yalla.testng.api.base.Annotations;

import cucumber.api.java.en.And;

public class LeadPage extends Annotations{ 

	public LeadPage() {
       PageFactory.initElements(driver, this);
	} 

	@FindBy(how=How.LINK_TEXT, using="Create Lead") WebElement elecreatelead;
	@And("click on create leads")
	public CreateLead clickCreateLead() {
		click(elecreatelead);  
		return new CreateLead();

	}

}







