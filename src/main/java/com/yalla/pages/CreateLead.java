package com.yalla.pages;

import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;
import org.openqa.selenium.support.PageFactory;

import com.yalla.testng.api.base.Annotations;

import cucumber.api.java.en.And;
import cucumber.api.java.en.Then;

public class CreateLead extends Annotations{ 
	
	public CreateLead() {
		PageFactory.initElements(driver, this); 
	}
	
	@FindBy(how=How.ID, using="createLeadForm_firstName")  WebElement eleFNAME;
	@FindBy(how=How.ID, using="createLeadForm_lastName")  WebElement eleLNAME;
	@FindBy(how=How.ID, using="createLeadForm_companyName") WebElement eleCompany;
	@FindBy(how=How.XPATH, using="//input[@class='smallSubmit']") WebElement elecreateLead;
	@And ("enter the first name as (.*)")
	public CreateLead enterFName(String data) {
		clearAndType(eleFNAME, data);  
		return this; 
	}
	@And("enter the company name as (.*)")
	public CreateLead enterCompany(String data) {
		clearAndType(eleCompany, data); 
		return this; 
	}
	@And("enter the last name as (.*)")
	public CreateLead enterLName(String data) {
		clearAndType(eleLNAME, data); 
		return this;
	}
	@And("click on submit")
	public ViewLead clickCreateLead() {
		click(elecreateLead);
		return new ViewLead(); 
	}

	@Then("verify the firstname")
	public void verifyTheFirstname() {
		String Fname = driver.findElement(By.className("tabletext")).getText();
		System.out.println(Fname.contentEquals("Sang"));
	

	
	}
}
	








