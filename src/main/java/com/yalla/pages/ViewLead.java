package com.yalla.pages;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;
import org.openqa.selenium.support.PageFactory;

import com.yalla.testng.api.base.Annotations;

public class ViewLead extends Annotations{ 

	public ViewLead() {
       PageFactory.initElements(driver, this);
	} 

	@FindBy(how=How.XPATH, using="(//span[@class='tabletext'])[4]") WebElement eleFirstName;
	public ViewLead VerifyFname(String data) {
		verifyExactText(eleFirstName, data);
		return this;

	}

}







